//
// Created by bogodan on 26.10.20.
//

#ifndef AOS_LAB2_PROCESSOR_H
#define AOS_LAB2_PROCESSOR_H

#include <vector>
#include <string>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <fstream>
#include <iostream>

class Processor {
private:
    enum class Command{
        mov,
        oper18
    };
    std::string file_name;

    static const int register_bitness = 32;

    std::string command_register;  // current command

    /// after parsing command info contains in next members
    std::string command_type;
    int mov_value;
    int mov_destination;            //
    int operand_for_nonmov_oper1;   // numbers of needed register
    int operand_for_nonmov_oper2;   //

    int status_register = 0;
    int number_command_register = 0;
    int number_tact_register = 0;

    void mov(int dec_number, int number_of_register);
    int operation18(int first_register, int second_register);   //operation im my variant(2.10.18)
    void parse_command(const std::string& command);
    void load_commands();
public:
    std::vector<std::string> commands_to_do;
    std::vector<std::vector<int>> data_registers;
    static std::vector<int> dec_to_bin_straight_code(int dec_number);
    static void straight_code_to_additional(std::vector<int> &st_code_number); // convert param to additional code
    void print_tact_info();
    explicit Processor(std::string &txt_input_file);
    void run();
    void do_command(std::string& command);

};


#endif //AOS_LAB2_PROCESSOR_H
