//
// Created by bogodan on 26.10.20.
//

#include "Processor.h"


Processor::Processor(std::string &txt_input_file) : data_registers(std::vector<std::vector<int>> {std::vector<int>(32, 0),
                                                                                                  std::vector<int>(32,0)}),
                                                    file_name(txt_input_file)
{
    load_commands();
}

void Processor::parse_command(const std::string& command) {
    int pos_of_space = command.find(' ');
    int pos_of_comma = command.find(',');
    command_register = command;
    command_type = command.substr(0,pos_of_space);
    if (command_type == "mov"){
        mov_destination = std::stoi(command.substr(pos_of_space+1, pos_of_comma-pos_of_space-1));
        mov_value = std::stoi(command.substr(pos_of_comma+1));
    }
    else{
        operand_for_nonmov_oper1 = std::stoi(command.substr(pos_of_space+1, pos_of_comma-pos_of_space-1));
        operand_for_nonmov_oper2 = std::stoi(command.substr(pos_of_comma+1));

    }
}

void Processor::mov(int dec_number, int number_of_register) {
    assert( (number_of_register <= 1) && (number_of_register >= 0) );
    data_registers[number_of_register] = dec_to_bin_straight_code(dec_number);
    status_register = data_registers[number_of_register][0]; // set sign of number in status register
    if (dec_number<0) {
        straight_code_to_additional(data_registers[number_of_register]);
    }

}

std::vector<int> Processor::dec_to_bin_straight_code(int dec_number) {
    std::vector<int> res;
    int sign = 0;
    assert(dec_number < pow(2, register_bitness)); // if number is too big for register with given register_bitness
    if (dec_number == 0){
        return std::vector<int>(register_bitness, 0);
    }
    else if (dec_number < 0){
        dec_number = abs(dec_number);
        sign = 1;
    }
    while (dec_number != 0){
        res.push_back(dec_number%2);
        dec_number = dec_number/2;
    }
    while(res.size() < register_bitness-1){
        res.push_back(0);
    }
    res.push_back(sign); // it will be first sign bit
    std::reverse(res.begin(), res.end());
    return res;
}

void Processor::straight_code_to_additional(std::vector<int> &st_code_number) {
    if (st_code_number != std::vector<int>(register_bitness,0)){
        st_code_number[0]=0;
        for(int & i : st_code_number){
            if (i == 0){
                i = 1;
            }
            else{
                i = 0;
            }
        }
        auto j = st_code_number.size()-1;
        while (st_code_number[j] == 1){
            st_code_number[j] = 0;
            j--;
        }
        st_code_number[j] = 1;
    }
}

void Processor::load_commands() {
    std::ifstream fin(file_name);
    char buff[64];
    assert(fin.is_open());
    while(!fin.eof()){
        fin.getline(buff,50);
        commands_to_do.emplace_back(buff);
    }
}

void Processor::print_tact_info() {
    std::cout << "command: " << command_register << std::endl;

    std::cout << "R0:";
    for (int i = 0; i < data_registers[0].size(); i++){
        /// do byte splitting
        if (i%8 == 0){
            std::cout << " ";
        }
        std::cout << data_registers[0][i];
    }
    std::cout << std::endl;

    std::cout << "R1:";
    for (int i = 0; i < data_registers[1].size(); i++){
        /// do byte splitting
        if (i%8 == 0){
            std::cout << " ";
        }
        std::cout << data_registers[1][i];
    }
    std::cout << std::endl;

    std::cout << "status_reg: " << status_register << std::endl;
    std::cout << "Ntack_reg: " << number_tact_register << std::endl;
    std::cout << "Ncomm_reg: " << number_command_register << std::endl;
    std::cout << "========================================";

}

void Processor::do_command(std::string &command) {
    parse_command(command);
    if (command_type == "mov"){
        mov(mov_value,mov_destination);
        number_tact_register++;
//        number_command_register++;
        print_tact_info();
        char tmp[1];
        std::cin.getline(tmp,1); // wait for view next tact(do next command)
    }
    else{
        mov_value = operation18(operand_for_nonmov_oper1, operand_for_nonmov_oper2);
        mov_destination = 0;
        number_tact_register++;
        status_register = (mov_value > 0)?0:1;
        print_tact_info();
        char tmp[1];
        std::cin.getline(tmp,1); // wait for view next tact(do next command)
        mov(mov_value,mov_destination);
        number_tact_register++;
//        number_command_register++;
        print_tact_info();
        char tmp1[1];
        std::cin.getline(tmp1,1); // wait for view next tact(do next command)
    }
}

void Processor::run() {
    for (auto command: commands_to_do){
//        std::string tmp;
//        std::cin >> tmp;
        number_command_register++;
        do_command(command);
        number_tact_register=0;
    }

}

int Processor::operation18(int first_register, int second_register) {
    assert( (first_register <= 1) && (first_register >= 0) );
    assert( (second_register <= 1) && (second_register >= 0) );
    int value_of_byte_number = 0;
    int sum_of_numbers = 0;
    int mod = 0;
    ///let sum of digits in second register be a module of division
    for (auto j: data_registers[second_register]){
        mod+=j;
    }
    ///translate each byte into a decimal digit and calculate their sum
    for(int i = 0; i < register_bitness; i++){
        if (i%8 == 0){
            sum_of_numbers += value_of_byte_number%10;
            value_of_byte_number=0;
        }
        value_of_byte_number += static_cast<int>((data_registers[first_register][i] * pow(2,7-(i%8) )));

    }
    return sum_of_numbers%mod;
}
